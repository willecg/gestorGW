#!/usr/bin/env python

# gestorGW: gestorGW.py
#
# Prueba varias puertas de enlace y establece la primer que de salida adecuada.
#
# Copyright (C) 2015 William Ernesto Cárdenas Gómez <willecg@openmailbox.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import os
import time
import threading

#------------------------------------------------------------------------------
class Probador(threading.Thread):
    """Realiza pruebas de conectividad.

    Se almacenan las posibles puertas de enlace y se van probando de una en una
    hasta que se detecta la que es correcta.
    Se deben especificar:
     · Una dirección de red.
     · La mascara de red.
     · Una dirección ip de prueba (Es decir de un equipo de cómputo).
    """
    sleepTime = 600
    _puertas = []

    def __init__(self,
            dir_red = '127.0.0.1',
            mascara = '/32',
            ip_prueba='127.0.0.1'):
        """Constructor inicializa la clase.
        """
        threading.Thread.__init__(self)
        self.setDirRed(dir_red)
        self.setMascara(mascara)
        self.setIpPrueba(ip_prueba)
        self.indice = 0

    # Funciones set
    def setDirRed(self, dir_red):
        """Establece la dirección de red destino.
        """
        self._dirRed = dir_red

    def setMascara(self, mascara):
        """Establece la mascara.

           Para pruebas en Linux con iproute6 la mascara es CIDR.
           Para windows se usa mascara en formato decimal"""
        self._mascara = mascara

    def setIpPrueba(self, ip_prueba):
        """Establece la IP que se usará para pruebas
        """
        self._ipPrueba = ip_prueba
    
    # funciones add
    def addPuerta(self, puerta):
        """Agrega a la lista un elemento mas.

        El elemento agregado es una ip que se usará como puerta de enlace.
        """
        self._puertas.append(puerta)

    # funciones get
    def getDirRed(self):
        """Obtiene la dirección de red.
        """
        return self._dirRed

    def getMascara(self):
        """Obtiene la mascara de red
        """
        return self._mascara

    def getIpPrueba(self):
        """Obtener la dirección IP para pruebas
        """
        return self._ipPrueba
    
    def nextPuerta(self):
        """Regresa la siguiente puerta de enlace.
        
           Si ya se llegó a la última se vuelve a iniciar."""
        i = self.indice
        if(self.indice + 1 == len(self._puertas)):
            self.indice = 0
        else:
            self.indice = self.indice + 1
        return self._puertas[i]

    def run(self):
        """Ejecución del hilo
        """
        while True:
            exito = os.system('ping -c1 ' + self.getIpPrueba())

            if(exito == 0):
                print('Bién ;)')
                time.sleep(self.sleepTime)
            else:
                exito = os.system('ip route del ' +
                        self.getDirRed() + self.getMascara())
                exito = os.system('ip route add ' +
                        self.getDirRed() + '' +
                        self.getMascara() + ' via ' +
                        self.nextPuerta())

#------------------------------------------------------------------------------
class Parser:
    """Parsea y almacena un arbol sintactico.
    
       La información almacenada es obtenida y parseada desde el archivo de
       configuración."""

    def __init__(self, config):
        """Contructor."""
        self.puertas=[]
        self.direcciones=[]
        self.archivo = open(config, 'r')
        self.crearEstructura()

    # Funciones add-set
    def addPuerta(self, puerta):
        """Agrega una candidata a puerta de enlace."""
        self.puertas.append(puerta)

    def addDireccion(self, ip, mask, test):
        """Agrega una dirección destino."""
        addr = [ip, mask, test]
        self.direcciones.append(addr)

    # Funciones get
    def getPuertas(self):
        """Regresa la lista de puertas candidatas."""
        return self.puertas

    def getDirecciones(self):
        """Obtiene el listado de direcciones destino"""
        return self.direcciones
    
    # Funciones propias del proceso de parseo y análisis sintáctico
    #  Toda esta parte es muy simplista y no detecta errores de forma adecuada.
    #  El hackeo aquí es bienvenido.
    def tokenizar(self):
        """Tokeniza desde el archivo de configuracion.

            Esta función es la primera que utiliza el parser para crear el
            arbol sintáctico. Después de su ejecución se establece una lista
            con los tripletes listos para realizar el análisis sintáctico y
            listo para cargarse en el arbol sintáctico"""
        self.tokens = []
        i_linea = 0
        for linea in self.archivo:
            token = ''
            i_linea = i_linea + 1
            for i in range(len(linea)):
                if(linea[i] != ' ' and linea[i] != ',' and
                   linea[i] != '=' and linea[i] != '#' and
                   linea[i] != '\n'):
                    token = token + linea[i]
                else:
                    if(len(token) > 0):
                        triplete = [token, i_linea, i - len(token)]
                        self.tokens.append(triplete)
                    token = ''
                    if(linea[i] == ',' or linea[i] == '='):
                        triplete = [linea[i], i_linea, i]
                        self.tokens.append(triplete)
                    if(linea[i] == '#'):
                        break
    
    def crearEstructura(self):
        """Crea el arbol sintáctico y lo almacena en memoria.
        
           Llama en primer lugar al método tokenizar y a partir de la lista
           creada inicia el proceso de cargar en memoria el arbol
           sintáctico."""
        self.tokenizar()
        i = 0
        while i < len(self.tokens):
            if(self.tokens[i][0] == 'puertas'):
                i = i + 1
                if(self.tokens[i][0] == '='):
                    i = i + 1
                    l_index = self.tokens[i][1]
                    while True:
                        self.addPuerta(self.tokens[i][0])
                        if(     i + 1< len(self.tokens) and 
                                self.tokens[i + 1][0] == ','):
                            i = i + 2
                        else:
                            break
            elif(self.tokens[i][0] == 'direccion'):
                i = i + 1
                if(self.tokens[i][0] == '='):
                    i = i + 1
                    l_index = self.tokens[i][1]
                    self.addDireccion(self.tokens[i][0], self.tokens[i+2][0],
                            self.tokens[i + 4][0])
                    i = i + 4
            else:
                print(  "Token no válido: ", self.tokens[i][0],
                        "\nLinea:", self.tokens[i][1], "Columna:",
                        self.tokens[i][2],
                        "\nSaliendo con error.")
                exit(1)
            i = i + 1

# Proceso primario del script
 #Carga de datos
parser = Parser('gestorGW.conf')
puertas = parser.getPuertas()
direcciones = parser.getDirecciones()

# Preparacion de hilos o subprocesos
hilos = []
for direccion in direcciones:
    hilo = Probador(direccion[0], direccion[1], direccion[2])
    for puerta in puertas:
        hilo.addPuerta(puerta)
    hilos.append(hilo)

for hilo in hilos:
    hilo.start()

print('Hilos en ejecución')

for hilo in hilos:
    hilo.join()

print('Hilos terminados')
